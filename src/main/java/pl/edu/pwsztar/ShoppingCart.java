package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShoppingCart implements ShoppingCartOperation {

    private Map<String, Product> shoppingCart = new HashMap<>();
    private int numberOfProductsInCart = 0;

    public boolean addProducts(String productName, int price, int quantity) {
        if (price <= 0 || quantity <= 0 || productName == null || productName.isEmpty()
                || numberOfProductsInCart + quantity > PRODUCTS_LIMIT) {
            return false;
        }
        if (shoppingCart.containsKey(productName)) {
            if (shoppingCart.get(productName).getPrice() == price) {
                Product modifiedItem = shoppingCart.get(productName);
                modifiedItem.setQuantity(modifiedItem.getQuantity() + quantity);
                shoppingCart.replace(productName, modifiedItem);
                numberOfProductsInCart += quantity;
                return true;
            }
            return false;
        }
        Product newItem = new Product(productName, price, quantity);
        shoppingCart.put(productName, newItem);
        numberOfProductsInCart += quantity;
        return true;
    }

    public boolean deleteProducts(String productName, int quantity) {
        if (productName == null || productName.isEmpty() || quantity <= 0) {
            return false;
        }
        if (shoppingCart.containsKey(productName)) {
            if (getQuantityOfProduct(productName) - quantity >= 0) {
                Product modifiedItem = shoppingCart.get(productName);
                modifiedItem.setQuantity(modifiedItem.getQuantity() - quantity);
                shoppingCart.replace(productName, modifiedItem);
                numberOfProductsInCart -= quantity;
                if (getQuantityOfProduct(productName) == 0) {
                    shoppingCart.remove(productName);
                }
                return true;
            }
        }
        return false;
    }

    public int getQuantityOfProduct(String productName) {
        if (productName == null || productName.isEmpty()) {
            return 0;
        }
        if (shoppingCart.containsKey(productName)) {
            Product item = shoppingCart.get(productName);
            return item.getQuantity();
        }
        return 0;
    }

    public int getSumProductsPrices() {
        return shoppingCart.values().stream()
                .map(product -> product.getPrice() * product.getQuantity())
                .reduce(0, Integer::sum);
    }

    public int getProductPrice(String productName) {
        if (productName == null || productName.isEmpty() || shoppingCart.get(productName) == null) {
            return 0;
        }
        return shoppingCart.get(productName).getPrice();
    }

    public List<String> getProductsNames() {
        return new ArrayList<>(shoppingCart.keySet());
    }
}
