package pl.edu.pwsztar;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class GetProductPriceTest {

    static ShoppingCart shoppingCart;

    @BeforeEach
    void createCart() {
        shoppingCart = new ShoppingCart();
    }

    @ParameterizedTest
    @CsvSource({
            "Landrynki, 4, 10",
            "Galaretki, 5, 5",
            "Krówki, 6, 15"
    })
    void shouldReturnProductPrices(String productName, int price, int quantity) {
        shoppingCart.addProducts(productName, price, quantity);
        assertEquals(price, shoppingCart.getProductPrice(productName));
    }

    @Test
    void shouldReturnZeroIfThereIsNoSuchProductInCart() {
        assertEquals(0,shoppingCart.getProductPrice("Landrynki"));
    }

    @ParameterizedTest
    @CsvSource({
            "0",
            "-4",
            "NotExistingProduct",
            ","
    })
    void shouldReturnZeroIfArgumentIsInvalid(String productName) {
        assertEquals(0, shoppingCart.getProductPrice(productName));
    }
}
