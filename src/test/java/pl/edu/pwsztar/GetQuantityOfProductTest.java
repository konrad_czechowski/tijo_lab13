package pl.edu.pwsztar;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GetQuantityOfProductTest {

    static ShoppingCart shoppingCart;

    @BeforeEach
    void createCart() {
        shoppingCart = new ShoppingCart();
    }

    @Test
    void shouldReturnQuantityAfterAddingSomeProducts() {
        shoppingCart.addProducts("Cukierki", 5, 50);
        shoppingCart.addProducts("Wafelki", 6, 30);
        shoppingCart.addProducts("Cukierki", 5, 50);
        int quantity = shoppingCart.getQuantityOfProduct("Cukierki");
        assertEquals(100, quantity);
    }

    @Test
    void shouldReturnZeroIfProductIsNotInCart() {
        shoppingCart.addProducts("Cukierki", 5, 50);
        shoppingCart.addProducts("Wafelki", 6, 30);
        shoppingCart.addProducts("Cukierki", 5, 50);
        int quantity = shoppingCart.getQuantityOfProduct("Żelki");
        assertEquals(0, quantity);
    }

    @Test
    void shouldReturnZeroIfProductIsEmpty() {
        shoppingCart.addProducts("Cukierki", 5, 50);
        int quantity = shoppingCart.getQuantityOfProduct("");
        assertEquals(0, quantity);
    }

    @ParameterizedTest
    @CsvSource({
            "0",
            ",",
            "NotExistingProduct"
    })
    void shouldReturnZeroIfArgumentIsInvalid(String productName) {
        assertEquals(0, shoppingCart.getQuantityOfProduct(productName));
    }
}
