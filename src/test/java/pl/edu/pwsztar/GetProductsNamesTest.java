package pl.edu.pwsztar;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GetProductsNamesTest {

    static ShoppingCart shoppingCart;

    @BeforeAll
    static void createCart() {
        shoppingCart = new ShoppingCart();
    }

    @Test
    void shouldReturnNamesOfAllProductsInCart() {
        shoppingCart.addProducts("Wafelki", 5, 40);
        shoppingCart.addProducts("Ciastka", 7, 50);
        shoppingCart.addProducts("Cukierki", 3, 60);
        List<String> products = Arrays.asList("Ciastka", "Cukierki", "Wafelki");
        List<String> returnList = shoppingCart.getProductsNames();
        assertTrue(returnList.containsAll(products));
    }

    @Test
    void shouldReturnEmptyListIfCartIsEmpty() {
        assertEquals(Collections.emptyList(), shoppingCart.getProductsNames());
    }
}
