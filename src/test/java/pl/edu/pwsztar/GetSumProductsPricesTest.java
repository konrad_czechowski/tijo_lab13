package pl.edu.pwsztar;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GetSumProductsPricesTest {

    static ShoppingCart shoppingCart;

    @BeforeAll
    static void createCart() {
        shoppingCart = new ShoppingCart();
    }

    @Test
    void shouldReturnSumOfAllProductsPrices() {
        shoppingCart.addProducts("Cukierki", 5, 100);
        shoppingCart.addProducts("Galaretki", 2, 50);
        int sumValue = shoppingCart.getSumProductsPrices();
        assertEquals(600, sumValue);

    }
}
