package pl.edu.pwsztar;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class DeleteProductsTest {

    static ShoppingCart shoppingCart;

    @BeforeEach
    void createCart() {
        shoppingCart = new ShoppingCart();
    }

    @Test
    void shouldDeleteGivenQuantityOfProduct() {
        shoppingCart.addProducts("Czekoladki", 2, 10);
        shoppingCart.deleteProducts("Czekoladki", 4);
        assertEquals(6, shoppingCart.getQuantityOfProduct("Czekoladki"));
    }

    @Test
    void shouldCompletelyDeleteProductIfQuantityEqualsZero() {
        shoppingCart.addProducts("Żelki", 1, 5);
        boolean returnValue = shoppingCart.deleteProducts("Żelki", 5);
        assertTrue(returnValue && shoppingCart.getProductPrice("Żelki") == 0);
    }

    @Test
    void shouldNotDeleteIfGivenQuantityIsGreaterThanQuantityInCart() {
        shoppingCart.addProducts("Cukierki", 1, 5);
        boolean returnValue = shoppingCart.deleteProducts("Cukierki", 13);
        assertFalse(returnValue && shoppingCart.getQuantityOfProduct("Cukierki") == 5);
    }

    @ParameterizedTest
    @CsvSource({
            ",0",
            ",-4",
            "NotExistingProduct,2"
    })
    void shouldNotDeleteAnythingIfArgumentsAreInvalid(String productName, int quantity) {
        boolean returnValue = shoppingCart.deleteProducts(productName, quantity);
        assertFalse(returnValue);
    }
}
