package pl.edu.pwsztar;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AddProductsTest {

    static ShoppingCart shoppingCart;

    @BeforeEach
    void createCart() {
        shoppingCart = new ShoppingCart();
    }

    @Test
    void addOneItem() {
        Product mockedProduct = new Product("Krówki", 2, 40);
        boolean returnValue = shoppingCart.addProducts(mockedProduct.getName(), mockedProduct.getPrice(), mockedProduct.getQuantity());
        assertTrue(returnValue);
    }

    @ParameterizedTest
    @CsvSource({
            "Czekolada, 3, 1",
            "Wafelki, 2, 10",
            "Żelki, 1, 489"
    })
    void addItems(String productName, int price, int quantity) {
        boolean returnValue = shoppingCart.addProducts(productName, price, quantity);
        assertTrue(returnValue);
    }

    @Test
    void addTheSameItemMultipleTimes() {
        shoppingCart.addProducts("Czekolada", 3, 1);
        boolean returnValue2 = shoppingCart.addProducts("Czekolada", 3, 3);
        assertTrue(returnValue2);
    }

    @Test
    void addTheSameItemMultipleTimesButWithDifferentPrice() {
        shoppingCart.addProducts("Czekolada", 3, 1);
        boolean returnValue = shoppingCart.addProducts("Czekolada", 2, 3);
        assertFalse(returnValue);
    }

    @Test
    void tryAddingNegativeNumberOfItems() {
        boolean returnValue = shoppingCart.addProducts("Czekolada", 3, -1);
        assertFalse(returnValue);
    }

    @Test
    void tryAddingItemWithNegativePrice() {
        boolean returnValue = shoppingCart.addProducts("Czekolada", -5, 2);
        assertFalse(returnValue);
    }

    @Test
    void tryAddingItemWithNegativePriceAndNegativeQuantity() {
        boolean returnValue = shoppingCart.addProducts("Czekolada", -100, -4);
        assertFalse(returnValue);
    }

    @Test
    void tryAddingItemWithoutName() {
        boolean returnValue = shoppingCart.addProducts("", 4, 6);
        assertFalse(returnValue);
    }

    @Test
    void tryAddingItemWithZeroQuantity() {
        boolean returnValue = shoppingCart.addProducts("Napój", 5, 0);
        assertFalse(returnValue);
    }

    @Test
    void tryAddingTooMuchProducts() {
        shoppingCart.addProducts("Żelki", 1, 300);
        boolean returnValue = shoppingCart.addProducts("Cukierki", 2, 400);
        assertFalse(returnValue);
    }
}
